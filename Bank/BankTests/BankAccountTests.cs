﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankAccountNS;

namespace BankTests
{
    [TestClass]
    public class BankAccountTests
    {
        [TestMethod]
        public void Debit_WithValidAmount_UpdatesBalance()
        {
            double beginningBalance = 11.99;
            double debitAmount = 4.55;
            double expected = 7.44;
            BankAccount account = new BankAccount("Mr. Bryan Walton", beginningBalance);

            //act
            account.Debit(debitAmount);

            //Assert
            double actual = account.Balance;
            Assert.AreEqual(expected, actual, 000.1, "Your account not debited correcly");
        }

        //unit test method
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Debit_WhenAmountIsLessThanZero_ShouldThrowArguementOutOfRange()
        {
            double beginningBalance = 11.99;
            double debitAmount = 50.00;
            BankAccount account = new BankAccount("Mr Bryan Walton", beginningBalance);

            //act
            account.Debit(debitAmount);
        }//End of Method


        public const string DebitAmountExceedsBalanceMessage = "Debit amount exceeds Balance";
        public const string DebitAmountLessThanZeroMessage = "Debit amount less than Zero";

        public void Debit_WhenAmountIsMoreThanBalance_ShouldThrowArguementOutOfRange()
        {
            //arrange 
            double beginningBalance = 11.99;
            double debitAmount = 20.0;
            BankAccount account = new BankAccount("Mr Bryan Walton", beginningBalance);

            //act
            try
            {
                account.Debit(debitAmount);
            }
            catch (ArgumentOutOfRangeException e)
            {
                //Assert 
                StringAssert.Contains(e.Message, BankAccount.DebitAmountExceedsBalanceMessage);
                return;
            }
            Assert.Fail("No Exception was thrown");
        }
    }
}
