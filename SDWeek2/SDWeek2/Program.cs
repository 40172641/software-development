﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDWeek2
{
    class Program
    {
        static void Main(string[] args){

            //Find the Length and Width of the Hall
            Console.WriteLine("Enter the Length and Width of the Hall");
            int hallLength = Convert.ToInt32(Console.ReadLine());
            int hallWidth = Convert.ToInt32(Console.ReadLine());
            int hallArea = hallWidth * hallLength;

            //Find the Length and Width of the  Lounge 
            Console.WriteLine("Enter the Length and Width of the Lounge");
            int loungeWidth = Convert.ToInt32(Console.ReadLine());
            int loungeLength = Convert.ToInt32(Console.ReadLine());
            int loungeArea = loungeWidth * loungeLength;

            //Find the Length and Width of the Kitchen
            Console.WriteLine("Enter the Length and Width of the Kitchen");
            int kitchenLength = Convert.ToInt32(Console.ReadLine());
            int kitchenWidth = Convert.ToInt32(Console.ReadLine());
            int kitchenArea = kitchenLength * kitchenWidth;

            // Total Area
            int totalArea = kitchenArea + loungeArea + hallArea;

            //Output
            Console.WriteLine("Room" + '\t' + "Size (sq ms");
            Console.WriteLine("Lounge" + '\t' + loungeArea);
            Console.WriteLine("Kitchen" + '\t' + kitchenArea);
            Console.WriteLine("Hall" + '\t' + hallArea + '\n');
            Console.WriteLine("Total" + '\t' + totalArea);
            Console.ReadKey();

         
        }

       
    
    }
}
